/** @type {import('tailwindcss').Config} */
// Palette
// https://coolors.co/11393d-254d51-d0531e-839398-f0fdff
module.exports = {
  content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  theme: {
    colors: {
      transparent: "transparent",
      current: "currentColor",
      green: "#11393D",
      "light-green": "#254D51",
      orange: "#D0531E",
      white: "#F8FEFF",
      shadow: "#DDE1E4",
    },
    fontFamily: {
      sans: ["Poppins", "sans-serif"],
    },
    extend: {
      fontSize: {
        "3s": "0.422rem",
        "2s": "0.563rem",
        "1s": "0.75rem",
        "0x": "1rem",
        "1x": "1.333rem",
        "2x": "1.777rem",
        "3x": "2.369rem",
        "4x": "3.157rem",
        "5x": "4.209rem",
      },
    },
  },
  plugins: [],
};
