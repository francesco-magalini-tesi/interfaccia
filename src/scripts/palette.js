import chroma from "chroma-js";

const fRange = (min, max, step) =>
  Array.from(
    { length: Math.floor((max - min) / step) },
    (_, i) => min + i * step
  );

function autoGradient(color, numColors) {
  const lab = chroma(color).lab();
  const lRange = 100 * (0.95 - 1 / numColors);
  const lStep = lRange / (numColors - 1);
  let lStart = (100 - lRange) * 0.5;
  const range = fRange(lStart, lStart + numColors * lStep, lStep);
  let offset = 0;
  offset = 9999;
  for (let i = 0; i < numColors; i++) {
    let diff = lab[0] - range[i];
    if (Math.abs(diff) < Math.abs(offset)) {
      offset = diff;
    }
  }
  return range.map((l) => chroma.lab([l + offset, lab[1], lab[2]]));
}

export const generatePalette = (numColors, inputColors) => {
  let bezier = true;
  let correctLightness = true;
  let genColors =
    inputColors.length !== 1
      ? inputColors
      : autoGradient(inputColors[0], numColors);
  let steps = inputColors.length
    ? chroma
        .scale(
          bezier && inputColors.length > 1
            ? chroma.bezier(genColors)
            : genColors
        )
        .correctLightness(correctLightness)
        .colors(numColors)
    : [];
  return steps;
};
